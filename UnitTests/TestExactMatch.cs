﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FuzzyRegex;

namespace UnitTests
{
    [TestClass]
    public class TestExactMatch
    {
        [TestMethod]
        public void Test_foobar()
        {
            var re = new Automaton("foobar");

            var result = re.Match("foobar", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 6);
            Assert.AreEqual(result.Matches.Count, 1);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 6);

            result = re.Match("aaaafoobaraaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 4);
            Assert.AreEqual(result.Length, 6);
            Assert.AreEqual(result.Matches.Count, 1);
            Assert.AreEqual(result.Matches[0][0], 4);
            Assert.AreEqual(result.Matches[0][1], 10);

            result = re.Match("foobaraafoobar", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 6);
            Assert.AreEqual(result.Matches.Count, 1);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 6);

            result = re.Match("foobraafoobar", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 7);
            Assert.AreEqual(result.Length, 6);
            Assert.AreEqual(result.Matches.Count, 1);
            Assert.AreEqual(result.Matches[0][0], 7);
            Assert.AreEqual(result.Matches[0][1], 13);

            result = re.Match("fooxbar", 0);
            Assert.IsFalse(result.Success);

            result = re.Match("foobr", 0);
            Assert.IsFalse(result.Success);

            result = re.Match("foabar", 0);
            Assert.IsFalse(result.Success);

            result = re.Match("fooxbar", 0);
            Assert.IsFalse(result.Success);
        }

        [TestMethod]
        public void Test_a_star_star()
        {
            var re = new Automaton("(a*)*");

            var result = re.Match("", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 0);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 0);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);

            result = re.Match("aaaaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 7);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 7);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 7);

            result = re.Match("aaaaxaaaaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 4);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 4);

            result = re.Match("xxxxxxxxxxxxxxxxxxxxxxxa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 23);
            Assert.AreEqual(result.Length, 1);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 23);
            Assert.AreEqual(result.Matches[0][1], 24);
            Assert.AreEqual(result.Matches[1][0], 23);
            Assert.AreEqual(result.Matches[1][1], 24);
        }

        [TestMethod]
        public void Test_submatch1()
        {
            var re = new Automaton("((a)|(aa)|(aaa)|(a+))*");

            var result = re.Match("", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 0);
            Assert.AreEqual(result.Matches.Count, 6);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 0);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);
            Assert.AreEqual(result.Matches[4][0], -1);
            Assert.AreEqual(result.Matches[4][1], -1);
            Assert.AreEqual(result.Matches[5][0], -1);
            Assert.AreEqual(result.Matches[5][1], -1);

            result = re.Match("a", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 1);
            Assert.AreEqual(result.Matches.Count, 6);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 1);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 1);
            Assert.AreEqual(result.Matches[2][0], 0);
            Assert.AreEqual(result.Matches[2][1], 1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);
            Assert.AreEqual(result.Matches[4][0], -1);
            Assert.AreEqual(result.Matches[4][1], -1);
            Assert.AreEqual(result.Matches[5][0], -1);
            Assert.AreEqual(result.Matches[5][1], -1);

            result = re.Match("aa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 2);
            Assert.AreEqual(result.Matches.Count, 6);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 2);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 2);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], 0);
            Assert.AreEqual(result.Matches[3][1], 2);
            Assert.AreEqual(result.Matches[4][0], -1);
            Assert.AreEqual(result.Matches[4][1], -1);
            Assert.AreEqual(result.Matches[5][0], -1);
            Assert.AreEqual(result.Matches[5][1], -1);

            result = re.Match("aaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 3);
            Assert.AreEqual(result.Matches.Count, 6);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 3);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 3);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);
            Assert.AreEqual(result.Matches[4][0], 0);
            Assert.AreEqual(result.Matches[4][1], 3);
            Assert.AreEqual(result.Matches[5][0], -1);
            Assert.AreEqual(result.Matches[5][1], -1);

            result = re.Match("aaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 6);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 4);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 4);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);
            Assert.AreEqual(result.Matches[4][0], -1);
            Assert.AreEqual(result.Matches[4][1], -1);
            Assert.AreEqual(result.Matches[5][0], 0);
            Assert.AreEqual(result.Matches[5][1], 4);

            result = re.Match("xxaaaaaaaaaaaaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 2);
            Assert.AreEqual(result.Length, 15);
            Assert.AreEqual(result.Matches.Count, 6);
            Assert.AreEqual(result.Matches[0][0], 2);
            Assert.AreEqual(result.Matches[0][1], 17);
            Assert.AreEqual(result.Matches[1][0], 2);
            Assert.AreEqual(result.Matches[1][1], 17);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);
            Assert.AreEqual(result.Matches[4][0], -1);
            Assert.AreEqual(result.Matches[4][1], -1);
            Assert.AreEqual(result.Matches[5][0], 2);
            Assert.AreEqual(result.Matches[5][1], 17);
        }

        [TestMethod]
        public void Test_submatch2()
        {
            var re = new Automaton("(a+|(aa)|(aaa))*");

            var result = re.Match("", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 0);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 0);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("a", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 1);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 1);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 1);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("aa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 2);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 2);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 2);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("aaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 3);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 3);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 3);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("aaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 4);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 4);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("xxaaaaaaaaaaaaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 2);
            Assert.AreEqual(result.Length, 15);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 2);
            Assert.AreEqual(result.Matches[0][1], 17);
            Assert.AreEqual(result.Matches[1][0], 2);
            Assert.AreEqual(result.Matches[1][1], 17);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

        }

        [TestMethod]
        public void Test_submatch3()
        {
            var re = new Automaton("(a?|(aa)|(aaa))*");

            var result = re.Match("", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 0);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 0);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("a", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 1);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 1);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 1);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("aa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 2);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 2);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 2);
            Assert.AreEqual(result.Matches[2][0], 0);
            Assert.AreEqual(result.Matches[2][1], 2);
            Assert.AreEqual(result.Matches[3][0], -1);
            Assert.AreEqual(result.Matches[3][1], -1);

            result = re.Match("aaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 3);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 3);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 3);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], 0);
            Assert.AreEqual(result.Matches[3][1], 3);

            result = re.Match("aaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 4);
            Assert.AreEqual(result.Matches[1][0], 1);
            Assert.AreEqual(result.Matches[1][1], 4);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], 1);
            Assert.AreEqual(result.Matches[3][1], 4);

            result = re.Match("xxaaaaaaaaaaaaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 2);
            Assert.AreEqual(result.Length, 15);
            Assert.AreEqual(result.Matches.Count, 4);
            Assert.AreEqual(result.Matches[0][0], 2);
            Assert.AreEqual(result.Matches[0][1], 17);
            Assert.AreEqual(result.Matches[1][0], 14);
            Assert.AreEqual(result.Matches[1][1], 17);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);
            Assert.AreEqual(result.Matches[3][0], 14);
            Assert.AreEqual(result.Matches[3][1], 17);
        }

        [TestMethod]
        public void Test_submatch4()
        {
            var re = new Automaton("((a*)b)*b");

            var result = re.Match("", 0);
            Assert.IsFalse(result.Success);

            result = re.Match("b", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 1);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 1);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);

            result = re.Match("aaabbb", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 6);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 6);
            Assert.AreEqual(result.Matches[1][0], 4);
            Assert.AreEqual(result.Matches[1][1], 5);
            Assert.AreEqual(result.Matches[2][0], 4);
            Assert.AreEqual(result.Matches[2][1], 4);

            result = re.Match("xxxxxxbxxxx", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 6);
            Assert.AreEqual(result.Length, 1);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 6);
            Assert.AreEqual(result.Matches[0][1], 7);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);

            result = re.Match("xxabbbxxaaaaabb", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 2);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 2);
            Assert.AreEqual(result.Matches[0][1], 6);
            Assert.AreEqual(result.Matches[1][0], 4);
            Assert.AreEqual(result.Matches[1][1], 5);
            Assert.AreEqual(result.Matches[2][0], 4);
            Assert.AreEqual(result.Matches[2][1], 4);

            result = re.Match("baaaabb", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 7);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 7);
            Assert.AreEqual(result.Matches[1][0], 1);
            Assert.AreEqual(result.Matches[1][1], 6);
            Assert.AreEqual(result.Matches[2][0], 1);
            Assert.AreEqual(result.Matches[2][1], 5);            
        }

        [TestMethod]
        public void Test_a_star_aaaa()
        {
            var re = new Automaton("(a)*aaaa");

            var result = re.Match("", 0);
            Assert.IsFalse(result.Success);

            result = re.Match("aaaaaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 8);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 8);
            Assert.AreEqual(result.Matches[1][0], 3);
            Assert.AreEqual(result.Matches[1][1], 4);

            result = re.Match("aaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 4);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);

            result = re.Match("aaaxxxaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 6);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 6);
            Assert.AreEqual(result.Matches[0][1], 10);
            Assert.AreEqual(result.Matches[1][0], -1);
            Assert.AreEqual(result.Matches[1][1], -1);

            result = re.Match("aaaaaxxxaaaaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 5);
            Assert.AreEqual(result.Matches.Count, 2);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 5);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 1);
        }

        [TestMethod]
        public void Test_caret_a_star_dollar()
        {
            var re = new Automaton("^((a)*)$");

            var result = re.Match("", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 0);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 0);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 0);
            Assert.AreEqual(result.Matches[2][0], -1);
            Assert.AreEqual(result.Matches[2][1], -1);

            result = re.Match("a", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 1);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 1);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 1);
            Assert.AreEqual(result.Matches[2][0], 0);
            Assert.AreEqual(result.Matches[2][1], 1);

            result = re.Match("aaaa", 0);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(result.ErrorsCount, 0);
            Assert.AreEqual(result.BeginPos, 0);
            Assert.AreEqual(result.Length, 4);
            Assert.AreEqual(result.Matches.Count, 3);
            Assert.AreEqual(result.Matches[0][0], 0);
            Assert.AreEqual(result.Matches[0][1], 4);
            Assert.AreEqual(result.Matches[1][0], 0);
            Assert.AreEqual(result.Matches[1][1], 4);
            Assert.AreEqual(result.Matches[2][0], 3);
            Assert.AreEqual(result.Matches[2][1], 4);

            result = re.Match("aaaax", 0);
            Assert.IsFalse(result.Success);

            result = re.Match("xaaaa", 0);
            Assert.IsFalse(result.Success);

            result = re.Match("aaaxaaa", 0);
            Assert.IsFalse(result.Success);
        }
    }
}

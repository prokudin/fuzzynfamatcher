﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyRegex
{
    class Program
    {
        static void Main(string[] args)
        {
            //var re = new Automaton("^((a|b|c)(b|c|d|e))+$");
            //re.ToDot();
            //var tmp = re.Match("adda",2);
            //tmp = re.Match("", 2);

            var re = new Automaton("(a*)*");
            re.ToDot();
            var tmp = re.Match("adda", 2);
            tmp = re.Match("", 2);

            //re = new Automaton("^(a*)+$");
            //re.ToDot();
            //tmp = re.Match("aaaaa");
            //tmp = re.Match("aaaaaxaa",1);
            //tmp = re.Match("aaaaaax",1);
            //tmp = re.Match("xxaaaaaaaax",3);


        }
    }
}

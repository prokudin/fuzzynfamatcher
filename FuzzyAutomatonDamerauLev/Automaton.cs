﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyRegex
{
    public class Automaton
    {
        public Automaton(string pattern)
        {
            var builder = new AutomatonBuilder(this);

            builder.Build(pattern);
        }

        private Dictionary<int, List<Transition>> transitions = new Dictionary<int, List<Transition>>();
        private Dictionary<int, int> inputCount = new Dictionary<int, int>();
        private List<int> initialNodes = new List<int>();
        private List<int> finalNodes = new List<int>();
        private List<int> submatchTags = new List<int>();
        private int currentPos = 0;
        private int maxLev = 0;
        private int tagsCount = 0;
        private State emptyState;
        private State finalState;
        private bool finalStateFound = false;
        private string String;
        private int Length;

        //состояния автомата
        private Dictionary<int, State> reach = new Dictionary<int, State>();
        private Dictionary<int, State> reachNext = new Dictionary<int, State>();

        public MatchResult Match(string str, int maxErrors = 0)
        {
            finalStateFound = false;
            finalState = null;
            maxLev = maxErrors;
            String = str;
            Length = str.Length;

            int i = 0;
            do
            {
                Simulate(i);
                i++;
            } while (i < Length);
            
            return CreateMatchResultFromState(finalState);
        }

        private MatchResult CreateMatchResultFromState(State state)
        {
            if (state == null)
                return new MatchResult();

            List<int[]> matches = new List<int[]>(submatchTags.Count);
            for (int i = 0; i < submatchTags.Count; i++)
            {
                matches.Add(new int[] { state.OpenedTags[submatchTags[i]], state.ClosedTags[submatchTags[i]] });
            }

            return new MatchResult(true, state.BeginPos, state.Length, matches,
                state.lev, state.ins, state.sub, state.del, state.trns,
                state.insertions, state.substitutions, state.deletions, state.transpositions);
        }


        private State Simulate(int beginPos = 0)
        {
            reach.Clear();
            reachNext.Clear();
            currentPos = beginPos - 1;
            emptyState.BeginPos = beginPos;

            // add initial states
            foreach (var id in initialNodes)
                reach.Add(id, emptyState.Clone(id));

            //epsClosure for initialStates
            EpsClosure(reach);

            bool isActive = Length > 0 && currentPos < Length - 1;
            while (isActive)
            {// пока в автомате есть состояния
                reachNext.Clear();
                currentPos++;

                foreach (var state in reach)
                {// для каждого состояния
                    foreach (var trans in transitions[state.Key])
                    {// для каждого перехода из узла текущего состояния
                        if (!trans.ConsumesChar())
                            continue;

                        bool compare = trans.Compare(String[currentPos]);

                        if (!compare && state.Value.lev >= maxLev)
                            continue;

                        int to = trans.NextID;
                        var newState = state.Value.Clone(to);
                        newState.Length++;

                        foreach (var tagid in trans.OpenedTags)
                            newState.OpenedTags[tagid] = currentPos;
                        foreach (var tagid in trans.ClosedTags)
                            newState.ClosedTags[tagid] = currentPos + 1;                        

                        if (!compare)
                        {// псевдопереход замены
                            Mark subst = new Mark(MarkType.SUBSTITUTION, currentPos, trans.Char);
                            newState.sub++;
                            newState.lev++;
                            newState.substitutions.Add(subst);
                        }

                        if (!reachNext.ContainsKey(to) || newState.LeftmostLongest(reachNext[to]))
                        {// если состояние выигрывает
                            newState.PreviousCharTransition = trans;
                            reachNext[to] = newState;

                            if (finalNodes.Contains(to) && (!finalStateFound || newState.LeftmostLongest(finalState)))
                            {// если состояние в финальном узле
                                finalStateFound = true;
                                finalState = newState;
                                maxLev = newState.lev;
                            }
                        }
                    }

                    //псевдопереходы удаления
                    if (state.Value.lev < maxLev)
                    {
                        var to = state.Key;
                        var newState = state.Value;
                        newState.lev++;
                        newState.del++;
                        newState.Length++;
                        Mark del = new Mark(MarkType.DELETION, currentPos, String[currentPos]);
                        newState.deletions.Add(del);

                        if (!reachNext.ContainsKey(to) || newState.LeftmostLongest(reachNext[to]))
                        {
                            reachNext[to] = newState;

                            if (finalNodes.Contains(to) && (!finalStateFound || newState.LeftmostLongest(finalState)))
                            {// если состояние в финальном узле
                                finalStateFound = true;
                                finalState = newState;
                                maxLev = newState.lev;
                            }
                        }
                    }
                }

                isActive = reachNext.Count > 0 && currentPos < Length - 1;

                EpsClosure(reachNext);

                //swap reach and reachNext
                var tmp = reach;
                reach = reachNext;
                reachNext = tmp;
            }

            return finalStateFound ? finalState : null;
        }

        private void EpsClosure(Dictionary<int, State> reach)
        {
            // количество входных дуг для кажого узла
            var inputCount = new Dictionary<int, int>(this.inputCount);

            LinkedList<int> queue = new LinkedList<int>(reach.Keys);
            while (queue.Count > 0)
            {
                int from = queue.First.Value;
                queue.RemoveFirst();

                foreach (var trans in transitions[from])
                {
                    bool consumesChar = trans.ConsumesChar();
                    if (consumesChar && reach[from].lev > maxLev)  /// !!!!!!!!! мб изменено в случае транспозиций, в случае изменения этого условия добавить эту проверку для случая конкуренции
                        continue;

                    int to = trans.NextID;
                    var newState = reach[from].Clone(to);

                    //tags
                    foreach (var tagid in trans.OpenedTags)
                        newState.OpenedTags[tagid] = currentPos + 1;
                    foreach (var tagid in trans.ClosedTags)
                        newState.ClosedTags[tagid] = currentPos + 1;

                    if (consumesChar)
                    {
                        if (newState.PreviousCharTransition != null
                            && currentPos >= 1
                            && newState.PreviousCharTransition.Compare(String[currentPos])
                            && trans.Compare(String[currentPos - 1])
                            /*&& newState.del > 0 && newState.deletions[newState.del - 1].Position == currentPos - 1*/)
                        {// если транспозиция
                            Console.WriteLine("ku");
                            Mark trns = new Mark(MarkType.TRANSPOSITION, currentPos - 1, trans.Char);
                            newState.trns++;
                            newState.lev++;
                            newState.transpositions.Add(trns);
                                                        
                            if (newState.del > 0 && newState.deletions[newState.del - 1].Position == currentPos - 1)
                            {// удалить последнее удаление
                                newState.del--;
                                newState.lev--;
                                newState.deletions.RemoveAt(newState.del);
                            }
                        }
                        else
                        {// если псевдопереход вставки
                            Mark ins = new Mark(MarkType.INSERTION, currentPos + 1, trans.Char);
                            newState.ins++;
                            newState.lev++;
                            newState.insertions.Add(ins);
                        }
                    }

                    if (trans.Type == TransitionType.DOLLAR)
                    {// если переход доллар
                        if (newState.lev + Length - (currentPos + 1) > maxLev || Length > 0 && currentPos + 1 == newState.BeginPos)
                            continue;

                        for (int i = currentPos + 1; i < Length; i++)
                        {// вставить символы в конец
                            Mark ins = new Mark(MarkType.INSERTION, i, String[i]);
                            newState.ins++;
                            newState.lev++;
                            newState.insertions.Add(ins);
                        }
                    }

                    if (trans.Type == TransitionType.CARET)
                    {// если переход каретка
                        if (newState.lev + newState.BeginPos > maxLev)
                            continue;

                        for (int i = 0; i < newState.BeginPos; i++)
                        {// вставить символы в конец
                            Mark ins = new Mark(MarkType.INSERTION, i, String[i]);
                            newState.ins++;
                            newState.lev++;
                            newState.insertions.Add(ins);
                        }
                    }

                    if (!reach.ContainsKey(to) || newState.LeftmostLongest(reach[to]))
                    {
                        if (consumesChar)
                            newState.PreviousCharTransition = trans;

                        reach[to] = newState;

                        inputCount[to]--;
                        if (inputCount[to] == 0)
                        {
                            queue.AddFirst(to);
                            inputCount[to] = this.inputCount[to];
                        }
                        else
                        {
                            queue.AddLast(to);
                        }

                        if (finalNodes.Contains(to) && (!finalStateFound || finalState.Length == 0 && newState.Length != 0 || newState.LeftmostLongest(finalState)))
                        {
                            finalStateFound = true;
                            finalState = newState;
                            maxLev = newState.lev;
                        }
                    }
                }
            }
        }


        private void AddTransition(Transition trans)
        {
            if (transitions.ContainsKey(trans.PrevID))
            {
                transitions[trans.PrevID].Add(trans);
            }
            else
            {
                transitions.Add(trans.PrevID, new List<Transition>() { trans });
            }

            if (inputCount.ContainsKey(trans.NextID))
                inputCount[trans.NextID]++;
            else
                inputCount[trans.NextID] = 1;

            if (!transitions.ContainsKey(trans.NextID))
                transitions.Add(trans.NextID, new List<Transition>());
        }

        public void ToDot()
        {
            string dotString = "digraph { \n rankdir = LR;";

            foreach (var id in initialNodes)
            {
                dotString += "\n\"" + id + "\"[shape = rarrow];";
            }

            foreach (var transition in transitions)
            {
                foreach (var trans in transition.Value)
                {
                    dotString += "\n" + trans.ToDotString();
                }
            }

            foreach (var id in finalNodes)
            {
                dotString += "\n\"" + id + "\"[shape = doublecircle];";
            }

            dotString += "\n}";

            using (StreamWriter bw = new StreamWriter(File.Create(@"C:\Users\Admin\YandexDisk\FuzzyAutomatonDamerauLev\a.txt")))
            {
                bw.Write(dotString);
                bw.Close();
            }
        }


        class AutomatonBuilder
        {
            private Automaton automaton;

            private int MaxNodeId = 0;

            private static Dictionary<char, int> operationPriority = new Dictionary<char, int>
            {
                ['*'] = 3,
                ['+'] = 3,
                ['?'] = 3,
                ['\x0345'] = 2,
                ['|'] = 1
            };

            public AutomatonBuilder(Automaton aut)
            {
                automaton = aut;
            }

            private bool IsOperator(char chr)
            {
                return operationPriority.ContainsKey(chr);
            }

            private bool IsQuantifier(char ch)
            {
                return ch == '*' || ch == '+' || ch == '?';
            }

            private string AddConcatenationCharacter(string original)
            {
                for (int i = 0; i < original.Length - 1; i++)
                {
                    if ((char.IsLetterOrDigit(original[i]) || original[i] == '^' || original[i] == '$') && (char.IsLetterOrDigit(original[i + 1]) || original[i + 1] == '^' || original[i + 1] == '$')
                        || original[i] == ')' && (char.IsLetterOrDigit(original[i + 1]) || original[i + 1] == '^' || original[i + 1] == '$')
                        || IsQuantifier(original[i]) && (char.IsLetterOrDigit(original[i + 1]) || original[i + 1] == '^' || original[i + 1] == '$')
                        || (char.IsLetterOrDigit(original[i]) || original[i] == '^' || original[i] == '$') && original[i + 1] == '('
                        || original[i] == ')' && original[i + 1] == '(')
                    {
                        original = original.Insert(i + 1, "\x0345");
                    }
                }

                return original;
            }

            public void Build(string regex)
            {
                regex = AddConcatenationCharacter(regex);

                // создание начальных переходов с нулевыми тэгами
                int initialId1 = MaxNodeId++;
                int initialId2 = MaxNodeId++;
                int finalId1 = MaxNodeId++;
                int finalId2 = MaxNodeId++;

                // Добавить тэги нулевой подмаски
                automaton.tagsCount++;
                automaton.AddTransition(Transition.EpsTransition(initialId1, initialId2, new List<int>() { 0 }));
                automaton.AddTransition(Transition.EpsTransition(finalId2, finalId1, null, new List<int>() { 0 }));
                automaton.submatchTags.Add(0);

                automaton.initialNodes.Add(initialId1);
                automaton.finalNodes.Add(finalId1);

                ProcessRegex(initialId2, finalId2, regex);

                // create emptyState
                automaton.emptyState = new State(automaton.initialNodes[0], automaton.tagsCount);
            }


            private void ProcessRegex(int prevNode, int nextNode, string regex)
            {
                while (IsSubmatch(regex))
                {// если подмаска
                    int newPrev = MaxNodeId++;
                    int newNext = MaxNodeId++;

                    automaton.AddTransition(Transition.EpsTransition(prevNode, newPrev, new List<int>() { automaton.tagsCount }));
                    automaton.AddTransition(Transition.EpsTransition(newNext, nextNode, null, new List<int>() { automaton.tagsCount }));

                    automaton.submatchTags.Add(automaton.tagsCount);
                    automaton.tagsCount++;

                    prevNode = newPrev;
                    nextNode = newNext;

                    regex = regex.Substring(1, regex.Length - 2);
                }

                int minPriorityOp = FindOperationWithMinPriority(regex);

                if (minPriorityOp == -1)
                {// если нашло символ
                    if (regex[0] == '^')
                    {
                        automaton.AddTransition(Transition.CaretTranstion(prevNode, nextNode));
                        return;
                    }

                    if (regex[0] == '$')
                    {
                        automaton.AddTransition(Transition.DollarTranstion(prevNode, nextNode));
                        return;
                    }

                    automaton.AddTransition(Transition.CharTransition(prevNode, nextNode, regex[0], new List<int>() { automaton.tagsCount }, new List<int>() { automaton.tagsCount }));

                    automaton.tagsCount++;

                    return;
                }

                int tmp1;
                int tmp2;
                switch (regex[minPriorityOp])
                {
                    case '|':
                        tmp1 = MaxNodeId++;
                        tmp2 = MaxNodeId++;
                        automaton.AddTransition(Transition.EpsTransition(prevNode, tmp1, new List<int>() { automaton.tagsCount }));
                        automaton.AddTransition(Transition.EpsTransition(tmp2, nextNode, null, new List<int>() { automaton.tagsCount }));
                        automaton.tagsCount++;
                        ProcessRegex(tmp1, tmp2, regex.Substring(0, minPriorityOp));

                        tmp1 = MaxNodeId++;
                        tmp2 = MaxNodeId++;
                        automaton.AddTransition(Transition.EpsTransition(prevNode, tmp1, new List<int>() { automaton.tagsCount }));
                        automaton.AddTransition(Transition.EpsTransition(tmp2, nextNode, null, new List<int>() { automaton.tagsCount }));
                        automaton.tagsCount++;
                        ProcessRegex(tmp1, tmp2, regex.Substring(minPriorityOp + 1));
                        break;
                    case '\x0345':
                        int newNode = MaxNodeId++;
                        ProcessRegex(prevNode, newNode, regex.Substring(0, minPriorityOp));
                        ProcessRegex(newNode, nextNode, regex.Substring(minPriorityOp + 1));
                        break;
                    case '?':
                        automaton.AddTransition(Transition.EpsTransition(prevNode, nextNode));
                        tmp1 = MaxNodeId++;
                        tmp2 = MaxNodeId++;
                        automaton.AddTransition(Transition.EpsTransition(prevNode, tmp1, new List<int>() { automaton.tagsCount }));
                        automaton.AddTransition(Transition.EpsTransition(tmp2, nextNode, null, new List<int>() { automaton.tagsCount }));
                        automaton.tagsCount++;
                        ProcessRegex(tmp1, tmp2, regex.Substring(0, minPriorityOp));
                        break;
                    case '+':
                        tmp1 = MaxNodeId++;
                        tmp2 = MaxNodeId++;
                        automaton.AddTransition(Transition.EpsTransition(prevNode, tmp1, new List<int>() { automaton.tagsCount }));
                        automaton.AddTransition(Transition.EpsTransition(tmp2, nextNode, null, new List<int>() { automaton.tagsCount }));
                        automaton.AddTransition(Transition.EpsTransition(tmp2, tmp1));
                        automaton.tagsCount++;
                        ProcessRegex(tmp1, tmp2, regex.Substring(0, minPriorityOp));
                        break;
                    case '*':
                        tmp1 = MaxNodeId++;
                        tmp2 = MaxNodeId++;

                        automaton.AddTransition(Transition.EpsTransition(prevNode, nextNode));
                        automaton.AddTransition(Transition.EpsTransition(prevNode, tmp1));
                        automaton.AddTransition(Transition.EpsTransition(tmp2, nextNode));
                        automaton.AddTransition(Transition.EpsTransition(tmp2, tmp1));
                        ProcessRegex(tmp1, tmp2, regex.Substring(0, minPriorityOp));
                        break;
                }
            }

            private bool IsSubmatch(string regex)
            {
                if (string.IsNullOrEmpty(regex) || regex[0] != '(' || regex[regex.Length - 1] != ')')
                    return false;

                int branches = 0;
                for (int i = 1; i < regex.Length - 1; i++)
                {
                    if (regex[i] == '(')
                        branches++;
                    if (regex[i] == ')')
                        branches--;

                    if (branches < 0)
                        return false;
                }

                return branches == 0;
            }


            private int FindOperationWithMinPriority(string regex)
            {
                int currentPriority = 10;
                int branches = 0;
                int minOpPos = -1;
                int i = -1;
                foreach (var chr in regex)
                {
                    i++;

                    if (IsOperator(chr) && branches == 0)
                    {
                        if (operationPriority[chr] < currentPriority)
                        {
                            currentPriority = operationPriority[chr];
                            minOpPos = i;
                        }
                    }

                    if (chr == '(')
                        branches++;

                    if (chr == ')')
                        branches--;
                }

                return minOpPos;
            }
        }
    }
}

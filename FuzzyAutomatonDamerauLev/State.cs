﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyRegex
{
    class State
    {
        public State(int nodeID,int tagsCount = 0)
        {
            NodeID = nodeID;

            lev = 0;
            del = 0;
            sub = 0;
            ins = 0;
            trns = 0;

            insertions = new List<Mark>();
            deletions = new List<Mark>();
            substitutions = new List<Mark>();
            transpositions = new List<Mark>();
            OpenedTags = Enumerable.Repeat(-1, tagsCount).ToList(); 
            ClosedTags = Enumerable.Repeat(-1, tagsCount).ToList();
        }

        private State(State clonable,int nodeID)
        {
            NodeID = nodeID;

            lev = clonable.lev;
            del = clonable.del;
            sub = clonable.sub;
            ins = clonable.ins;
            trns = clonable.trns;

            insertions = new List<Mark>(clonable.insertions);
            deletions = new List<Mark>(clonable.deletions);
            substitutions = new List<Mark> (clonable.substitutions);
            transpositions = new List<Mark>(clonable.transpositions);
            OpenedTags = new List<int>(clonable.OpenedTags);
            ClosedTags = new List<int>(clonable.ClosedTags);

            BeginPos = clonable.BeginPos;
            Length = clonable.Length;

            PreviousCharTransition = clonable.PreviousCharTransition;
        }


        public int NodeID               { get; private set; }

        public int lev; 
        public int del;
        public int sub;
        public int ins;
        public int trns;

        public int BeginPos = 0;
        public int Length = 0;

        public List<Mark> insertions     { get; private set; }
        public List<Mark> deletions      { get; private set; }
        public List<Mark> substitutions  { get; private set; }
        public List<Mark> transpositions { get; private set; }

        public List<int> OpenedTags     { get; private set; }
        public List<int> ClosedTags     { get; private set; }

        public Transition PreviousCharTransition = null;

        public State Clone(int newNodeId)
        {
            return new State(this, newNodeId);
        }

        public bool LeftmostLongest(State other)
        {
            if (lev < other.lev)
                return true;
            else if (lev > other.lev)
                return false;

            int length = Length + ins - del;
            int otherlength = other.Length + other.ins - other.del;

            if (length != 0 && (otherlength == 0 || BeginPos < other.BeginPos))
                return true;
            else if (length != 0 && (otherlength == 0 || BeginPos > other.BeginPos))
                return false;

            if (length > otherlength)
                return true;
            else if (length < otherlength)
                return false;

            if (trns > other.trns)
                return true;
            else if (trns < other.trns)
                return false;

            if (sub > other.sub)
                return true;
            else if (sub < other.sub)
                return false;

            if (ins > other.ins)
                return true;
            else if (ins < other.ins)
                return false;
            
            for (int i = 0, size = OpenedTags.Count; i < size; i++)
            {
                if (OpenedTags[i] != -1 && ClosedTags[i] != -1 && other.OpenedTags[i] == -1 && other.ClosedTags[i] == -1)
                {
                    for (int j = i + 1; j< size; j++)
                    {
                        if (OpenedTags[j] == -1 && ClosedTags[j] == -1 && other.OpenedTags[j] != -1 && other.ClosedTags[j] != -1)
                        {
                            return (ClosedTags[i] - OpenedTags[i] >= other.ClosedTags[j] - other.OpenedTags[j]);
                        }
                    }
                }

                if (OpenedTags[i] == -1 && ClosedTags[i] == -1 && other.OpenedTags[i] != -1 && other.ClosedTags[i] != -1)
                {
                    for (int j = i + 1; j < size; j++)
                    {
                        if (OpenedTags[j] != -1 && ClosedTags[j] != -1 && other.OpenedTags[j] == -1 && other.ClosedTags[j] == -1)
                        {
                            return (other.ClosedTags[i] - other.OpenedTags[i] < ClosedTags[j] - OpenedTags[j]);
                        }
                    }
                }

                if (OpenedTags[i] < other.OpenedTags[i])
                    return true;
                if (OpenedTags[i] > other.OpenedTags[i])
                    return false;
                if (ClosedTags[i] > other.ClosedTags[i])
                    return true;
                if (ClosedTags[i] < other.ClosedTags[i])
                    return false;
            }

            return false;
        }
    }
}

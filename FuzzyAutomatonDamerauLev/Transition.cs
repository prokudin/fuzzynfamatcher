﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyRegex
{
    enum TransitionType
    {
        CHAR,
        EPS,
        DOLLAR,
        CARET
    }
    
    class Transition
    {
        public TransitionType Type  { get; private set; }
        public int PrevID           { get; private set; }
        public int NextID           { get; private set; }
        public char Char            { get; private set; }
        public List<int> OpenedTags { get; private set; }
        public List<int> ClosedTags { get; private set; }

        public Transition(TransitionType type, int from, int to, char chr = 'ε',List<int> openedTags = null, List<int> closedTags = null)
        {
            Type = type;
            PrevID = from;
            NextID = to;
            OpenedTags = openedTags ?? new List<int>();
            ClosedTags = closedTags ?? new List<int>();
            Char = chr;
        }


        public static Transition EpsTransition(int from,int to, List<int> openedTags = null, List<int> closedTags = null)
        {
            return new Transition(TransitionType.EPS, from, to, 'ε', openedTags, closedTags);
        }

        public static Transition CharTransition(int from, int to,char chr, List<int> openedTags = null, List<int> closedTags = null)
        {
            return new Transition(TransitionType.CHAR, from, to, chr, openedTags, closedTags);
        }

        public static Transition DollarTranstion(int from, int to,List<int> openedTags = null, List<int> closedTags = null)
        {
            return new Transition(TransitionType.DOLLAR, from, to, '$', openedTags, closedTags);
        }

        public static Transition CaretTranstion(int from, int to, List<int> openedTags = null, List<int> closedTags = null)
        {
            return new Transition(TransitionType.CARET, from, to, '^', openedTags, closedTags);
        }

        public bool ConsumesChar()
        {
            return Type == TransitionType.CHAR;
        }

        public bool Compare(char chr)
        {
            switch (Type)
            {
                case TransitionType.CHAR:
                    return Char == chr;                
            }

            return false;
        }

        public string ToDotString()
        {
            string result = "" + PrevID + "->" + NextID + "[label = \"o:";

            foreach(var val in OpenedTags)
            {
                result += "" + val + ",";
            }

            result += " " + Char + " c:";

            foreach (var val in ClosedTags)
            {
                result += "" + val + ",";
            }

            result += "\"];";

            return result;
        }
    }
}

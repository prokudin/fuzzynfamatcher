﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyRegex
{
    public enum MarkType
    {
        DELETION,
        SUBSTITUTION,
        INSERTION,
        TRANSPOSITION
    }

    public class Mark
    {
        public int Position;
        public MarkType Type;
        public char Char;

        public Mark(MarkType type,int pos,char chr = '\0')
        {
            Position = pos;
            Type = type;
            Char = chr;
        }
    }
}

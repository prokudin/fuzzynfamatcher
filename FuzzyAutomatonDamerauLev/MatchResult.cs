﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyRegex
{
    public class MatchResult
    {
        public bool Success { get; private set; }

        public int BeginPos { get; private set; }
        public int Length { get; private set; }

        public List<int[]> Matches { get; private set; }

        public int ErrorsCount { get; private set; }

        public int InsertionsCount { get; private set; }
        public int SubstiutionsCount { get; private set; }
        public int DeletionsCount { get; private set; }
        public int TranspositionsCount { get; private set; }

        public Dictionary<MarkType, List<Mark>> Errors { get; private set; }

        public MatchResult(bool success=false,int beginPos=-1, int length=-1, List<int[]> matches=null, 
            int errorsCount=0, int insertionsCount=0, int substiutionsCount=0, int deletionsCount=0, int transpositionsCount=0,
            List<Mark> insertions = null, List<Mark> substiutions=null, List<Mark> deletions = null, List<Mark> transpositions = null)
        {
            Success = success;
            BeginPos = beginPos;
            Length = length;
            Matches = matches;
            ErrorsCount = errorsCount;
            InsertionsCount = insertionsCount;
            SubstiutionsCount = substiutionsCount;
            DeletionsCount = deletionsCount;
            TranspositionsCount = transpositionsCount;

            Errors = new Dictionary<MarkType, List<Mark>>();
            Errors[MarkType.DELETION] = deletions ?? new List<Mark>();
            Errors[MarkType.INSERTION] = insertions ?? new List<Mark>();
            Errors[MarkType.SUBSTITUTION] = substiutions ?? new List<Mark>();
            Errors[MarkType.TRANSPOSITION] = transpositions ?? new List<Mark>();
        }


    }     
}
